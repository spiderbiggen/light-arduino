
typedef struct Schedule {
  unsigned long interval;
  unsigned long next;
  void (*execute)();
  bool active;
} Schedule;

class Scheduler {
  public:
    Scheduler (Schedule tasks[], int length) {
      tasks_ = tasks;
      length_ = length;
    }

    static Schedule create_task(void (*execute)(), unsigned long interval = 0, bool immediate = false, unsigned long start_delay = 0) {
      unsigned long next = (immediate ? 0 : interval) + start_delay;
      return {
        .interval = interval,
        .next = next,
        .execute = execute,
        .active = interval != 0 || immediate == true
      };
    }

    void delay_task(void (*execute)(), unsigned long extend) {
      for (int i = 0; i < length_; i ++) {
        if(tasks_[i].execute == execute) {
          tasks_[i].next += extend;
          tasks_[i].active = true;
          Serial.println(String("Delayed task by seconds: " + extend));
          return;
        }
      }
    }

    void loop() {
      for (int i = 0; i < length_; i ++) {
        tasks_[i];
        if (tasks_[i].active && tasks_[i].next <= millis()) {
          tasks_[i].execute();
          if (tasks_[i].interval == 0){
            tasks_[i].active = false;
          }
          tasks_[i].next = millis() + tasks_[i].interval;
        }
      }
    }

  private:
    int length_;
    Schedule* tasks_;
};
