#include <Servo.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiManager.h> 

#include "Config.h"
#include "Interval.h"
#include "StateMachine.h"
#include "arduino_project.h"
// Initializing LED Pin
#define SERVO_PIN D0
#define LDR_PIN A0
#define BUTTON_PIN_IN D2


// Sensor Update interval
#define UPDATE_INTERVAL 60000
// Save State interval every half hour
#define SAVE_STATE_INTERVAL 1800000
// Revert to auto state after one hour;
#define REVERT_DELAY 3600000

const char *mqtt_host = MQTT_HOST;
const char *sensor_id = (const char *)calloc(11, sizeof(char));
const char *publish_data = (const char *)calloc(27, sizeof(char));
const char *receive_data = (const char *)calloc(28, sizeof(char));

byte data_buffer[3];
int light;
int last_button_val = 0;


Schedule tasks[] = {
  Scheduler::create_task(&update_light, UPDATE_INTERVAL, true),
  Scheduler::create_task(&save_state, SAVE_STATE_INTERVAL),
  Scheduler::create_task(&set_auto)
};

Servo servo;
WiFiClient esp_client;
PubSubClient client(esp_client);
WiFiManager wifiManager;
Scheduler scheduler(tasks, 3);
StateMachine state(&servo);

void setup() {
  Serial.begin(115200); 
  generateChipID();

  pinMode(LDR_PIN, INPUT);
  pinMode(BUTTON_PIN_IN, INPUT_PULLUP);
  last_button_val = digitalRead(BUTTON_PIN_IN);
  state.setup();
  servo.attach(SERVO_PIN);

  Serial.print("ID: ");
  Serial.println(sensor_id);
  
  wifiManager.autoConnect(sensor_id);
  client.setServer(mqtt_host, 1883);
  client.setCallback(callback);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  check_button();
  scheduler.loop();
  delay(10);
}

void check_button() {
  int val = digitalRead(BUTTON_PIN_IN);
  if(val != last_button_val && val == HIGH) {
    state.set(FORCE_OPEN - ((state.get() + 1) % 2));
    scheduler.delay_task(&set_auto, REVERT_DELAY);
    update_light();
  }
  last_button_val = val;
}

void update_light() {
  light = analogRead(LDR_PIN);
  if (light > 700)
    state.set(OPEN);
  else if (light < 600)
    state.set(CLOSED);
  else if (state.get() == AUTO)
    state.set(CLOSED);
  
  data_buffer[0] = (light & 0xFF00) >> 8;
  data_buffer[1] = light & 0x00FF;
  data_buffer[2] = state.get() & 0x00FF;

  client.publish(publish_data, data_buffer, 3);
}

void save_state() {
  state.flush();
}

void set_auto() {
  state.set(AUTO);
}

void generateChipID() {
  Serial.println(String("ids: ") + ESP.getChipId() + ", " + ESP.getFlashChipId());
  snprintf((char *)sensor_id, 11 * sizeof(char), "0x%04X%04X", ESP.getChipId(), ESP.getFlashChipId());
  snprintf((char *)publish_data, 27 * sizeof(char), "iot/device/%s/data", sensor_id);
  snprintf((char *)receive_data, 28 * sizeof(char), "iot/device/%s/state", sensor_id);
}

void callback(char* t, byte* message, unsigned int length) {
  String topic = String(t);
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  if (topic.endsWith("state")) {
    Serial.println(message[0]);
    state.set(message[0]);
    update_light();
    scheduler.delay_task(&set_auto, REVERT_DELAY);
    return;
  }
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(sensor_id)) {
      Serial.println("connected");
      // Subscribe
      client.subscribe(receive_data);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
