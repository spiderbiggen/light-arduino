#include <ESP_EEPROM.h>
#include <Servo.h>


#define AUTO 0
#define CLOSED 1
#define OPEN 2
#define FORCE_CLOSED 3
#define FORCE_OPEN 4

#ifndef MIN_MS
  #define MIN_MS 550
#endif
#ifndef MAX_MS
  #define MAX_MS 2550
#endif

#ifndef CLOSED_ANGLE
  #define CLOSED_ANGLE 0
#endif
#ifndef OPENED_ANGLE
  #define OPENED_ANGLE 180
#endif

class StateMachine {
  private:
    uint8_t state_ = AUTO;
    Servo *servo_;

    void set_angle(int angle) {
      servo_->writeMicroseconds(map(angle,0, 180, MIN_MS, MAX_MS));
    }

    void apply() {
      if (state_ == AUTO) {
        servo_->writeMicroseconds(-1);
      } else if (state_ % 2 == 0) {
        set_angle(OPENED_ANGLE);
      } else {
        set_angle(CLOSED_ANGLE);
      }
      Serial.println(state_);
    }

  public:
    StateMachine(Servo *servo) {
      servo_ = servo;
    }

    void setup() {
      EEPROM.begin(sizeof(uint8_t));
      load();
      apply();
    }

    void load() {
      EEPROM.get(0, state_);
    }

    void flush() {
      EEPROM.put(0, state_);
      EEPROM.commit();
    }
  
    void set(int state) {
      Serial.print(state_);
      Serial.print("__");
      Serial.println(state);
      if (state < AUTO || state > FORCE_OPEN) return;
      if (state_ == state) return;
      if (state_ >= FORCE_CLOSED && state != AUTO && state < FORCE_CLOSED) return;
      
      state_ = state;
      apply();
    }

    int get() {
      return state_;
    }
};
