# Light Arduino
Arduino code for iot project.
Initial intention for the project was to open/close curtains automatically or remotely based on the outside light intensity or user input.
Due to time constraints the project is only a crude approximation using an LDR and servo instead of a more proper lux sensor and motor setup.